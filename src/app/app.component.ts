import { Component } from '@angular/core';

@Component({
  selector: 'fb-root',
  template: `
    <button routerLink="users">users</button>
    <button routerLink="items">items</button>
    <router-outlet></router-outlet>
  `,
})
export class AppComponent  {

}
