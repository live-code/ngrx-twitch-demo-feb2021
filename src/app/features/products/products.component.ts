import { Component, OnInit, ViewChild } from '@angular/core';
import { Store } from '@ngrx/store';
import { addItem, addItemSuccess, deleteItem, loadItems } from './store/items/items.actions';
import { Item } from '../../model/item';
import { Observable } from 'rxjs';
import { getItems, getItemsError } from './store/items/items.selectors';
import { NgForm } from '@angular/forms';
import { Actions, ofType } from '@ngrx/effects';

@Component({
  selector: 'fb-items',
  template: `
    <div *ngIf="itemsError$ | async">c'è un errore</div>

    <form #f="ngForm" (submit)="addItemHandler(f)">
      <input type="text" name="name" [ngModel]>
    </form>

    <li *ngFor="let item of (items$ | async)">
      {{item.name}} - {{item.id}}
      <button (click)="deleteItemHandler(item.id)">delete</button>
    </li>

  `,
  styles: []
})
export class ProductsComponent implements OnInit {
  @ViewChild('f') form: NgForm;
  items$: Observable<Item[]> = this.store.select(getItems);
  itemsError$: Observable<boolean> = this.store.select(getItemsError);

  constructor(
    private store: Store,
    private actions$: Actions,
  ) {
    actions$
      .pipe(ofType(addItemSuccess))
      .subscribe(() => {
        this.form.reset();
      });
  }

  ngOnInit(): void {
    this.store.dispatch(loadItems());
  }

  addItemHandler(form: NgForm): void {
    this.store.dispatch(addItem({ item: form.value }));
  }

  deleteItemHandler(id: number): void {
    this.store.dispatch(deleteItem({ id }));
  }

}
