import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProductsRoutingModule } from './products-routing.module';
import { ProductsComponent } from './products.component';
import { FormsModule } from '@angular/forms';
import { ActionReducerMap, StoreModule } from '@ngrx/store';
import { itemsReducer, ItemsState } from './store/items/items.reducer';
import { EffectsModule } from '@ngrx/effects';
import { ItemsEffects } from './store/items/items.effects';

export interface ProductsState {
  items: ItemsState;
}

export const reducers: ActionReducerMap<ProductsState> = {
  items: itemsReducer,
};

@NgModule({
  declarations: [ProductsComponent],
  imports: [
    CommonModule,
    ProductsRoutingModule,
    FormsModule,
    StoreModule.forFeature('products', reducers),
    EffectsModule.forFeature([ItemsEffects])
  ]
})
export class ProductsModule { }
