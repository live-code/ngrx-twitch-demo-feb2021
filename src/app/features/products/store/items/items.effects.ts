import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { HttpClient } from '@angular/common/http';
import { catchError, concatMap, filter, map, mergeMap, switchMap, tap } from 'rxjs/operators';
import {
  addItem,
  addItemFail,
  addItemSuccess,
  deleteItem, deleteItemFail,
  deleteItemSuccess,
  loadItems,
  loadItemsFail,
  loadItemsSuccess
} from './items.actions';
import { Item } from '../../../../model/item';
import { fromEvent, of } from 'rxjs';

@Injectable()
export class ItemsEffects {

  loadItems$ = createEffect(() => this.actions$.pipe(
    ofType(loadItems),
    switchMap(
      () => this.http.get<Item[]>('http://localhost:3000/items')
        .pipe(
          map(items => loadItemsSuccess({ items })),
          catchError(() => of(loadItemsFail()))
        )
    )
  ));

  addItem$ = createEffect(() => this.actions$.pipe(
    ofType(addItem),
    concatMap(
      action => this.http.post<Item>('http://localhost:3000/items', action.item)
        .pipe(
          map(item => addItemSuccess({ item })),
          catchError(() => of(addItemFail()))
        )
    )
  ));

  deleteItem$ = createEffect(() => this.actions$.pipe(
    ofType(deleteItem),
    mergeMap(({ id }) => this.http.delete(`http://localhost:3000/items/${id}`)
        .pipe(
          map(() => deleteItemSuccess({ id })),
          catchError(() => of(deleteItemFail()))
        )
    )
  ));
/*
  deleteItemSuccess$ = createEffect(() => this.actions$.pipe(
    ofType(deleteItemSuccess),
    tap(() => {
      alert('redirect');
    })
  ), { dispatch: false});*/


  constructor(
    private actions$: Actions,
    private http: HttpClient
  ) {
  }
}
