import { ActionReducerMap, createReducer, on } from '@ngrx/store';
import { addItemSuccess, deleteItemSuccess, loadItemsFail, loadItemsSuccess } from './items.actions';
import { Item } from '../../../../model/item';

export interface ItemsState {
  list: Item[];
  error: boolean;
}

const initialState: ItemsState = {
  list: [],
  error: false
};

export const itemsReducer = createReducer(
  initialState,
  on(loadItemsSuccess, (state, action) => ({ list: [...action.items], error: false})),
  on(loadItemsFail, (state) => ({ list: [...state.list], error: true })),

  on(addItemSuccess, (state, action) => {
    return {
      list: [...state.list, action.item],
      error: false,
    };
  }),
  on(deleteItemSuccess, (state, action) => {
    return {
      list: state.list.filter(item => item.id !== action.id),
      error: false
    }
  }),
);
