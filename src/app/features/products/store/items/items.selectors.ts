import { ProductsState } from '../../products.module';
import { createFeatureSelector, createSelector } from '@ngrx/store';

export const getProductsFeature = createFeatureSelector<ProductsState>('products');

export const getItems = createSelector(
  getProductsFeature,
  state => state.items.list
);

export const getItemsError = createSelector(
  getProductsFeature,
  state => state.items.error
);
